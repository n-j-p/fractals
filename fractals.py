from scipy.misc import imshow
import os
import numpy as np
import PIL
os.environ['SCIPY_PIL_IMAGE_VIEWER']='c:\windows\system32\\rundll32.exe "C:\progra~1\window~4\PhotoViewer.dll", ImageView_Fullscreen'

X = 283
Y = 132
NC = 256

import math
SIN60 = math.sin(math.radians(60))
COS60 = 0.5


a=PIL.Image.new('RGB',(X,Y))

for x in range(X):
    for y in range(Y):
        a.putpixel((x,y),tuple(np.random.random_integers(0,NC-1,3)))
#imshow(a)


def drawLine(a, pt0, pt1, colour=(255,255,255), lw=1):
    '''Draws a line from pt0=(x0,y0) to pt1=(x1,y1) using 
    Bresenham's line algorithm
    (See wikipedia!)'''
    x0,y0=pt0
    x1,y1=pt1
    x0 = int(round(x0))
    y0 = int(round(y0))
    x1 = int(round(x1))
    y1 = int(round(y1))
    m,n=a.size
    dx = abs(x1-x0)
    dy = abs(y1-y0) 
    if x0 < x1:
        sx = 1
    else:
        sx = -1
    if y0 < y1:
        sy = 1
    else:
        sy = -1
    err = dx-dy
    width_range = range(-lw/2+1,lw/2+1)
    while True:

        if x0 >= 0 and x0 < m and y0 >= 0 and y0 < n:
            if dx >= dy:
                for j in width_range:
                    a.putpixel((x0,y0+j),colour)
            else:
                for j in width_range:
                    a.putpixel((x0+j,y0),colour)
        if x0 == x1 and y0 == y1:
            break
        e2 = 2*err
        if e2 > -dy:
            err = err - dy
            x0 = x0 + sx
        if e2 < dx:
            err = err + dx
            y0 = y0 + sy 

def vksGeneratePoints(pt1, pt2):
    v = (pt2[0]-pt1[0],pt2[1]-pt1[1])
    v3 = (v[0]/3,v[1]/3)
    r60v = (COS60 * v[0] + SIN60 * v[1],
            SIN60 * v[0] - COS60 * v[1])
    r60v3 = (r60v[0]/3,r60v[1]/3)
    
    p1 = (pt1[0]+v3[0],pt1[1]+v3[1])
    p3 = (p1[0] + r60v3[0], p1[1] - r60v3[1])
    p2 = (pt2[0]-v3[0],pt2[1]-v3[1])    
    return (p1, p3, p2)

def vksInitiator(imgsize):
    c = (imgsize/2,imgsize/2) # center of the plot

    # (x0,y0) is the top of the triangle
    x0 = c[0]
    y0 = c[1] - (imgsize+0.)/2

    base = math.sqrt(3)*imgsize/4
    height = (imgsize+0.)/4
    # (x1,y1) is the right-most point.
    x1 = c[0] + base
    y1 = c[1] + height

    # (x2,y2) is the left-most point.
    x2 = c[0] - base
    y2 = y1

    return [(x0,y0),(x1,y1),(x2,y2)]


def generatevonKochSnowflake(level,imgsize=100,colour=(0,)*3,lw=2):
    img = PIL.Image.new('RGB',(imgsize,)*2,color=(255,)*3)
    pts = vksInitiator(imgsize)
    d = math.sqrt((pts[0][0] - pts[1][0])**2 + (pts[0][1]-pts[1][1])**2)
    print('max. resolvable level approx. %d' % int(round(math.log(d,3))))

    pts.append(pts[0])
    while level > 0:
        N = len(pts)
        i = 0
        while i < N + 3 * (N-1) - 1:
            (p1,p3,p2) = vksGeneratePoints(pts[i], pts[i+1])
            pts.insert(i+1,p2)
            pts.insert(i+1,p3)
            pts.insert(i+1,p1)
            i += 4
        level -= 1
    # now draw lines between points:
    for i in range(len(pts)-1):
        drawLine(img, pts[i], pts[i+1], colour=colour,lw=2)
    drawLine(img, pts[i+1], pts[0], colour=colour,lw=2)
    pts.pop()
    return img

def vksRec(img, pt1, pt2, level):
    if level == 0:
        drawLine(img,pt1,pt2,(255,)*3)
    else:
        (p1,p3,p2) = vksGeneratePoints(pt1, pt2)
        vksRec(img, pt1, p1, level-1)
        vksRec(img, p1, p3, level-1)
        vksRec(img, p3, p2, level-1)
        vksRec(img, p2, pt2, level-1)
        
def generatevonKochSnowflakeRecursively(level,imgsize=100):
    img = PIL.Image.new('RGB',(imgsize,)*2)
    pts = vksInitiator(imgsize)
    pts.append(pts[0])
    for i in range(len(pts)-1):
        vksRec(img,pts[i],pts[i+1],level)
    return img


def drawTree(stem_height, stem_width, left_alpha, right_alpha, 
             left_angle, right_angle, level, imgsize=(500,500),
             start_colour = (60,32,0),
             end_colour = (0,102,0),
             s_angles = 0.):
    import math
    import numpy as np
    cmap = [start_colour]
    for i in range(1,level):
        f = (i+0.)/(level-1)
        c = (int(start_colour[0] + f * (end_colour[0] - start_colour[0])),
             int(start_colour[1] + f * (end_colour[1] - start_colour[1])),
             int(start_colour[2] + f * (end_colour[2] - start_colour[2])))
        cmap.append(c)
    def treeRec(img, cur_pos, cur_angle, cur_width, cur_height, cur_level):
        next_pos = (cur_pos[0] + cur_height * math.cos(math.radians(cur_angle)),
                    cur_pos[1] + cur_height * math.sin(math.radians(cur_angle)))
        if cur_level > 0:
            drawLine(img, cur_pos, next_pos, colour = cmap[level-cur_level],
                     lw = int(round(cur_width)))
            treeRec(img, next_pos, 
                    cur_angle + left_angle + np.random.randn()*s_angles,
                    cur_width * left_width_factor,
                    cur_height * left_height_factor, cur_level - 1)
            treeRec(img, next_pos, 
                    cur_angle - right_angle + np.random.randn()*s_angles,
                    cur_width * right_width_factor,
                    cur_height * right_height_factor, cur_level - 1)
    img = PIL.Image.new('RGB',imgsize, color = (255,)*3)
            
    cx = imgsize[0]/2

    if level > 0:
        drawLine(img, (cx, 0), (cx,stem_height), lw = stem_width, 
                 colour=cmap[0])
    left_width_factor   = pow(2, -1./left_alpha) 
    left_height_factor  = pow(2, -2./(3*left_alpha)) 
    right_width_factor  = pow(2, -1./left_alpha) 
    right_height_factor = pow(2, -2./(3*right_alpha))
    treeRec(img, (cx,stem_height), 
            90.+left_angle + np.random.randn()*s_angles, 
            stem_width * left_width_factor, 
            stem_height * left_height_factor, level-1)
    treeRec(img, (cx,stem_height), 
            90.-right_angle + np.random.randn()*s_angles,
            stem_width * right_width_factor, 
            stem_height * right_height_factor, level-1)
    
    return img
    

def _mandelbrot(Xmin=-2.0,Xmax=1.2,Ymin=-1.2,Ymax=1.2,imgsize=(320,240)):
    img = PIL.Image.new('RGB',imgsize, color = (255,)*3)
    m,n=imgsize
    MAX_SIZE = 4
    MAX_ITERS = 512
    deltaP = (Xmax - Xmin)/(n-1)
    Q = Ymin + (np.arange(m) + 0.)/(m-1)*(Ymax-Ymin)
    P = Xmin
    mx = 0
    pct = 0
    for i in range(n):
        new_pct = i * 100 / n
        if new_pct > pct:
            print (new_pct,'% complete')
        pct = new_pct

        for j in range(m):
            X = 0.
            Y = 0.
            Xsq = 0.
            Ysq = 0.
            iters = 0
            while Xsq + Ysq < MAX_SIZE and iters < MAX_ITERS:
                Xsq = X * X
                Ysq = Y * Y
                Y *= X
                Y += Y + Q[j]
                X = Xsq - Ysq + P
                iters += 1
            if iters == MAX_ITERS:
                img.putpixel((j,i),(0,)*3)
            else:
                img.putpixel((j,i),(255,iters % 256,iters % 256))

        P += deltaP
    print (mx)
    return img
            
def mandelbrot(Xmin=-2.0,Xmax=1.2,Ymin=-3.,Ymax=1.2,imgsize=(320,240),
               cmapstr='jet',ncolours=20):
    '''
    Mandelbrot map is zn+1 = zn**2 + c
    (zn = znr + znc.i,
    c = cr+cc.i)
    zn**2 = (znr+znc.i)**2
          = (znr**2 + i**2.znc**2 + 2znr.znc.i)
          = (znr**2 - znc**2) + 2znr.znc.i
    zn**2 + c = (znr**2 - znc**2 + cr) + (2znr.znc + cc).i

    In the code below, znr is X, and znc is Y, so that,
    and c = P+Q.i
    X <- X**2 - Y**2 + P
    Y <- 2X.Y + Q
    P ranges from Xmin to Xmax, 
    Q ranges from Ymin to Ymax.
    '''
    import numpy as np
    import matplotlib.pyplot as plt
    cmap = eval('plt.cm.'+cmapstr+'(np.arange(ncolours+0.)/(ncolours-1),bytes=True)[:,:3]')
#    cmap = [tuple(x) for x in np.random.random_integers(0,255,(10,3))]
    img = PIL.Image.new('RGB',imgsize, color = (255,)*3)
    maxr,maxc=imgsize
    MAX_SIZE = 4
    MAX_ITERS = 512

    #deltaP = (Xmin - Xmax)/(maxc-1)
    #Q = Ymax + (np.arange(maxr) + 0.)/(maxr-1)*(Ymin-Ymax)
    #P = Xmax
    P = Ymin + (Ymax - Ymin) * (np.arange(maxc)+0.)/(maxc-1)
    Q = Xmin + (Xmax - Xmin) * (np.arange(maxr)+0.)/(maxr-1)
    mx = 0
    pct = 0
    XY = np.zeros(2,dtype=np.float64)
    XY2 = np.zeros(2,dtype=np.float64)
    for col in range(maxc): # go down, then across
        new_pct = col * 100 / maxc
        if new_pct > pct:
            print (new_pct,'% complete')
        pct = new_pct

        for row in range(maxr):
            #X = 0.
            XY[0] = 0
            #Y = 0.
            XY[1] = 0
#            Xsq = 0.
            XY2[0] = 0
#            Ysq = 0.
            XY2[1] = 0
            iters = 0
#            while Xsq + Ysq < MAX_SIZE and iters < MAX_ITERS:
            while sum(XY2) < MAX_SIZE and iters < MAX_ITERS:
#                Xsq = X * X
                XY2[0] = XY[0] * XY[0]
#                Ysq = Y * Y
                XY2[1] = XY[1] * XY[1]
#                Y *= X
                XY[1] *= XY[0]
#                Y += Y + P[col]
                XY[1] += XY[1] + P[col]
#                X = Xsq - Ysq + Q[row]
                XY[0] = XY2[0] - XY2[1] + Q[row]
                iters += 1
            if iters == MAX_ITERS:
                img.putpixel((row,col),(0,)*3)
            else:
                img.putpixel((row,col),tuple(cmap[iters % ncolours]))

        #P += deltaP
    print (mx)
    return img
            
def mando(NP=10000,cmapstrinput='jet'):
#    def mandelbrot(Xmin=-2.0,Xmax=1.2,Ymin=-3.,Ymax=1.2,imgsize=(320,240),
#               cmapstr='jet',ncolours=20)=
    import matplotlib.pyplot as plt
    import numpy as np
    xi = -2.
    xx = 1.2
    yi = -1.5
    yx = 1.2
    NP = NP*1.
    ratio = 1 + 1./3
    #XR = 32*2
    #YR = 24*2
    YRt = np.sqrt(NP/ratio)
    YR = int(YRt)
    XR = int(NP/YRt)
#    for i in range(5):
    FIRST = True
    while True:
        import pdb
        pdb.set_trace()
        if FIRST and NP == 100000:
            img = PIL.Image.open('f:/mando100k.bmp')
        else:
            img=mandelbrot(xi,xx,yi,yx,
                           imgsize=(XR,YR),cmapstr=cmapstrinput)
        FIRST = False
        plt.imshow(img)
#        if NP==100000:
#            img.save('f:/mando100k.bmp',format='bmp')
        ax = plt.gca()
        ax.set_xticks(ax.get_xlim())
        ax.set_yticks(ax.get_ylim())
        xis = '%.5e' % xi
        xxs = '%.5e' % xx
        ax.set_xticklabels((xis,xxs))
        yis = '%.5e' % yi
        yxs = '%.5e' % yx
        ax.set_yticklabels((yis,yxs))
        rect = np.array(plt.ginput(2,timeout=0))
        xmin = np.min(rect[:,0])/XR
        xmax = np.max(rect[:,0])/XR
        ymin = (YR-np.max(rect[:,1]))/YR
        ymax = (YR-np.min(rect[:,1]))/YR
        print (xmin,xmax,ymin,ymax)
        #return rect

        xi2 = xmin*(xx-xi)+xi
        xx2 = xmax*(xx-xi)+xi
        yi2 = ymin*(yx-yi)+yi
        yx2 = ymax*(yx-yi)+yi

        print (xi2,xx2,yi2,yx2)

        xi = xi2
        xx = xx2
        yi = yi2
        yx = yx2
        ratio = (yx2-yi2)/(xx2-xi2)
        YRt = np.sqrt(NP/ratio)
        XR = int(YRt)
        YR = int(NP/YRt)
        

        print (xmin,xmax,ymin,ymax)
    
    
def julia_from_mando():
    import matplotlib.pyplot as plt
    import numpy as np
    img = PIL.Image.open('mando100k.bmp')
    NP = 10**5

    xi = -2.
    xx = 1.2
    yi = -1.5
    yx = 1.2

    ratio = 1 + 1./3

    YRt = np.sqrt(NP/ratio)
    YR = int(YRt)
    XR = NP//YRt

    print(XR,YR)
    
    plt.imshow(img)
    pt = np.array(plt.ginput(1,timeout=0))[0]
    pt2 = (pt[0]/XR * (xx-xi) + xi,
           -(pt[1]/YR * (yx-yi) + yi))
    return pt, pt2
